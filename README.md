# Tracker

A Pivotal Tracker clone in ember.js, used to learn the ember.js framework.

## Plan

(from http://emberjs.com/guides/getting-started/planning-the-application/)

1. It will display a list of tasks for the user to see. This list will be divided into three groups - Current, Backlog, and Icebox.
2. It has a button outside the list to create a new task. Clicking it will add a new unpersisted expanded task into the Icebox section.
3. Tasks in any section can be expanded to see a full view. Each task will have a name, a priority, an owner, a length, and a description.
4. Tasks in the Backlog will have a start button on them in their closed state. Clicking start will move the task to the Current display and set its status to started.
5. Tasks in the Current area will be able to have their status modified. From Started they can become Unstarted or Finished, from Finished they can be Delivered, from Delivered they can be Accepted or Rejected, and from Rejected they can be Started again.
6. Tasks can be reordered in their section, or moved from the Icebox to the Backlog and vice versa. This is done via a drag and drop mechanism.