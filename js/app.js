App = Ember.Application.create();

App.Router.map(function() {
  this.resource('categories', {path: '/'})
});

App.CategoriesRoute = Ember.Route.extend({
  model: function () {
    return this.store.find('category');
  }
});

App.CategoryController = Ember.ObjectController.extend({
  inflection: function () {
    var remaining = this.get('tasks.length');
    return remaining === 1 ? 'task' : 'tasks';
  }.property('tasks.@each')
});

App.TaskController = Ember.ObjectController.extend({
  actions: {
    edit: function() {
      this.set('isEditing', true);
    },

    cancel: function() {
      this.get('model').rollback();
      this.set('isEditing', false);
    },

    save: function() {
      this.get('model').save();
      this.set('isEditing', false);
    },

    delete: function() {
      this.get('model').deleteRecord();
      this.get('model').save();
    }
  },

  taskLengths: [
    {value: 'unknown', name: 'Unknown'},
    {value: 'short', name: 'Short'},
    {value: 'medium', name: 'Medium'},
    {value: 'long', name: 'Long'}
  ],

  users: function() {
    return this.store.findAll('user');
  }.property('name')
});

// Models
App.ApplicationAdapter = DS.FixtureAdapter.extend();

App.Category = DS.Model.extend({
  title: DS.attr('string'),
  tasks: DS.hasMany('task', {async: true}),

  css_class: function() {
    return this.get('title').toLowerCase()
  }.property('title')
})

App.Task = DS.Model.extend({
  title: DS.attr('string'),
  length: DS.attr('string'),
  owner: DS.belongsTo('user'),
  status: DS.attr('string'),
  description: DS.attr('string'),
  category: DS.belongsTo('category')
})

App.User = DS.Model.extend({
  name: DS.attr('string'),
  initials: DS.attr('string'),

  displayName: function() {
    return this.get('name') + ' ' + '(' + this.get('initials') + ')';
  }.property('name', 'initials')
})