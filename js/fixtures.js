App.Category.FIXTURES = [
  {
    id: 1,
    title: 'Current',
    tasks: [1, 2, 3, 4, 5, 6]
  },
  {
    id: 2,
    title: 'Backlog',
    tasks: [7, 8]
  },
  {
    id: 3,
    title: 'Icebox',
    tasks: [9]
  }
]

App.Task.FIXTURES = [
  {
    id: 1,
    title: 'Learn to program',
    length: 'long',
    owner: 1,
    status: 'accepted',
    description: 'The very basics to start off with.',
  },
  {
    id: 2,
    title: 'Be an awesome programmer',
    length: 'short',
    owner: 1,
    status: 'finished',
    description: 'A rockstar. Totally.'
  },
  {
    id: 3,
    title: 'Be an awesome Ember programmer',
    length: 'long',
    owner: 1,
    status: 'rejected',
    description: 'This is harder than it looks.'
  },
  {
    id: 4,
    title: 'Setup static layout',
    length: 'medium',
    owner: 1,
    status: 'started',
    description: 'Detailing the look and feel of the app before I make it interactive and emberfied.'
  },
  {
    id: 5,
    title: 'Eat all of the noms',
    length: 'long',
    owner: 2,
    status: 'started',
    description: 'Whaddya MEAN the bowl is empty!?'
  },
  {
    id: 6,
    title: 'Yell at Houdini',
    length: 'short',
    owner: 3,
    status: 'delivered',
    description: 'MROWOWOWOWOWOWOWOWOWOWOWOWOWOWOWOW',
  },
  {
    id: 7,
    title: 'Get task list loading from ember',
    length: 'unknown',
    owner: 1,
    description: ''
  },
  {
    id: 8,
    title: 'Add button to create new task',
    length: 'unknown',
    owner: 1,
    description: ''
  },
  {
    id: 9,
    title: 'Take over the WORLD',
    length: 'long',
    owner: null,
    description: 'Mwa. Mwaha. MWAHAHAHA.'
  }
]

App.User.FIXTURES = [
  {
    id: 1,
    name: 'Becky',
    initials: 'RS'
  },
  {
    id: 2,
    name: 'Monty',
    initials: 'ML'
  },
  {
    id: 3,
    name: 'Scooter',
    initials: 'SL'
  }
]